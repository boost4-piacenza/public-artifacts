#<a name="top"></a>BOOST 4.0 - Piacenza track and trace blockchain pilot

[![License badge](https://img.shields.io/hexpm/l/plug.svg)](https://opensource.org/licenses/Apache-2.0)

* [Introduction](#introduction)
* [License](#license)
* [Support](#support)

## Introduction
BOOST 4.0 Piacenza track and trace application implementation is an open-source blockchain based implementation of high-end textiles
manufacturing process tracking and tracing. The implementation is based on Hyperledger Fabric v1.4
 The application was developed in the scope of the following BOOST 4.0 EU project. 

[Top](#top)

## License
BOOST 4.0 Piacenza track and trace application is licenced under the Apache Licence Version 2.0. For more information see the LICENCE.md

[Top](#top)


