/*******************************************************************************
 * Copyright 2020 IBM
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
 

"use strict";

const { Contract } = require("fabric-contract-api");
const TrackedItem = require("./TrackedItem");
const Source = require("./Source");
const Document = require("./Document");

class TrackedItemContract extends Contract {
    async assetExists(ctx, assetKey) {
        const buffer = await ctx.stub.getState(assetKey);
        return !!buffer && buffer.length > 0;
    }

    async readAsset(ctx, assetKey) {
        const exists = await this.assetExists(ctx, assetKey);
        if (!exists) {
            throw new Error(`The asset ${assetKey} does not exist`);
        }
        const buffer = await ctx.stub.getState(assetKey);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async createTrackedItem(ctx, value) {
        value = JSON.parse(value);
        // Validate input
        const trackedItem = new TrackedItem(value, true);

        // Generate ledger key
        const key = trackedItem.getKey(ctx);

        const exists = await this.assetExists(ctx, key);
        if (exists) {
            throw new Error(`The tracked item ${key} already exists`);
        }

        // Check for quantity constraints
        if (trackedItem.sources.length > 0) {
            //No sources means it's a first entry. TBD
            let seenSources = [];
            for (let source of trackedItem.sources) {
                let sourceItem = new Source(source);
                let sourceKey = sourceItem.getKey(ctx);
                if (seenSources.includes(sourceKey)) {
                    throw new Error(
                        `Cannot use the same source twice. ${sourceKey} was already seen`
                    );
                }
                seenSources.push(sourceKey);
                if (await this.assetExists(ctx, sourceKey)) {
                    // Source exists, load it
                    const buffer = await ctx.stub.getState(sourceKey);
                    let asset = JSON.parse(buffer.toString());

                    // Check quantity and reduce it
                    asset.quantity = asset.quantity - sourceItem.quantity;
                    if (asset.quantity < 0) {
                        throw new Error(`Overconsumption of ${sourceKey}`);
                    }

                    //Store updated source
                    await ctx.stub.putState(
                        sourceKey,
                        Buffer.from(JSON.stringify(asset))
                    );
                } else {
                    throw new Error(`Source ${sourceKey} does not exists`);
                }
            }
        }

        // Store Documents
        if (trackedItem.documents.length > 0) {
            let seenDocuments = [];
            for (let rawDocument of trackedItem.documents) {
                let document = new Document(rawDocument, true);
                let documentKey = document.getKey(ctx);
                if (seenDocuments.includes(documentKey)) {
                    throw new Error(
                        `Same document ${documentKey} referenced twice`
                    );
                }
                seenDocuments.push(documentKey);

                await this.createDocument(ctx, JSON.stringify(rawDocument));

                //TODO: Store entire document copy in TrackedItem or only reference? For now, for simplicity, keeping the entire copy.
            }
        }

        // Store asset
        const buffer = Buffer.from(JSON.stringify(trackedItem));
        await ctx.stub.putState(key, buffer);

        // Extended validation rules
        //TODO: Currently hardcoded, need a more dynamic way to set them.

        // USA to YEMEN rule
        if (trackedItem.country === "USA") {
            if (
                await trackedItem.applyRecursiveBooleanRule(
                    ctx,
                    asset => asset.country === "YEM"
                )
            ) {
                throw new Error("Transaction breaks law #12345");
            }
        }
    }

    async addDocumentToTrackedItem(
        ctx,
        ownerId,
        productCode,
        lotNumber,
        rawDocument
    ) {
        //Get the existing tracked item
        let trackedItem = new TrackedItem(
            await this.readTrackedItem(ctx, ownerId, productCode, lotNumber),
            true
        );

        //Add document to the ledger
        await this.createDocument(ctx, rawDocument);

        // Check if document is already linked
        let document = new Document(JSON.parse(rawDocument), true);
        let documentKey = document.getKey(ctx);
        if (trackedItem.documents.length > 0) {
            let seenDocuments = [];
            for (let rawExistingDoc of trackedItem.documents) {
                let existingDoc = new Document(rawExistingDoc, true);
                seenDocuments.push(existingDoc.getKey(ctx));
            }
            if (seenDocuments.includes(documentKey)) {
                throw new Error(
                    `Same document ${documentKey} referenced twice`
                );
            }
        }

        //Add document to TrackedItem
        if (!document.isComplete()) {
            document = new Document(
                await this.readDocument(
                    ctx,
                    document.producer,
                    document.type,
                    document.number
                ),
                true
            );
        }
        trackedItem.documents.push(document.toJson());

        // Store asset
        await ctx.stub.putState(
            trackedItem.getKey(ctx),
            trackedItem.toString()
        );
    }

    async readTrackedItem(ctx, ownerId, productCode, lotNumber) {
        const placeholder = new TrackedItem({
            ownerId,
            productCode,
            lotNumber
        });
        const key = placeholder.getKey(ctx);
        return this.readAsset(ctx, key);
    }

    async readDocument(ctx, producer, type, number) {
        const placeholder = new Document({ producer, type, number });
        const key = placeholder.getKey(ctx);
        return this.readAsset(ctx, key);
    }

    async createDocument(ctx, value) {
        value = JSON.parse(value);
        let document = new Document(value, true);
        let documentKey = document.getKey(ctx);
        if (await this.assetExists(ctx, documentKey)) {
            // Document already exists
            if (document.isComplete()) {
                // Trying to overwrite. Make sure it is the exact same
                let existingDocument = new Document(
                    await this.readAsset(ctx, documentKey),
                    true
                );
                if (!document.compareTo(existingDocument)) {
                    throw new Error(
                        `${documentKey} already exists and is different`
                    );
                }
            }
            // Else: Nothing to do, the document exists
        } else {
            // First time seen this document, add it.
            if (document.isComplete()) {
                await ctx.stub.putState(
                    documentKey,
                    Buffer.from(JSON.stringify(value))
                );
            } else {
                throw new Error(`Document ${documentKey} is incomplete`);
            }
        }
    }

    // async updateTrackedItem(ctx, trackedItemId, newValue) {
    //     const exists = await this.assetExists(ctx, trackedItemId);
    //     if (!exists) {
    //         throw new Error(`The tracked item ${trackedItemId} does not exist`);
    //     }
    //     const asset = { value: newValue };
    //     const buffer = Buffer.from(JSON.stringify(asset));
    //     await ctx.stub.putState(trackedItemId, buffer);
    // }

    // async deleteTrackedItem(ctx, trackedItemId) {
    //     const exists = await this.assetExists(ctx, trackedItemId);
    //     if (!exists) {
    //         throw new Error(`The tracked item ${trackedItemId} does not exist`);
    //     }
    //     await ctx.stub.deleteState(trackedItemId);
    // }
}

module.exports = TrackedItemContract;
