/*******************************************************************************
 * Copyright 2020 IBM
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
 "use strict";

let Entity = require("./Entity");

class Document extends Entity {
    constructor(obj, enableValidation) {
        super(obj, enableValidation);
        Object.assign(this, obj);
    }

    getKey(ctx) {
        return ctx.stub.createCompositeKey("Document", [
            this.producer,
            this.type,
            this.number
        ]);
    }

    compareTo(document) {
        return (
            this.producer === document.producer &&
            this.type === document.type &&
            this.number === document.number &&
            this.date === document.date &&
            this.hash === document.hash &&
            this.hash_algorithm === document.hash_algorithm &&
            this.fileLocation === document.fileLocation
        );
    }

    isComplete() {
        return (
            this.date && this.hash && this.hash_algorithm && this.fileLocation
        );
    }
}

module.exports = Document;
