/*******************************************************************************
 * Copyright 2020 IBM
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
 "use strict";

let Entity = require("./Entity");
const Source = require("./Source");

class TrackedItem extends Entity {
    constructor(obj, enableValidation) {
        super(obj, enableValidation);
        Object.assign(this, obj);
    }

    getKey(ctx) {
        return ctx.stub.createCompositeKey("TrackedItem", [
            this.ownerId,
            this.productCode,
            this.lotNumber
        ]);
    }

    /**
     * Apply a boolean rule (function (TrackedItem) => boolean), recursively on the sources
     * Stops processing as soon as one returns true
     *
     * @param ctx
     * @param rule
     * @returns {Promise<void>}
     */
    async applyRecursiveBooleanRule(ctx, rule) {
        if (rule(this)) {
            return true;
        }
        if (Array.isArray(this.sources) && this.sources.length > 0) {
            for (let source of this.sources) {
                let sourceItem = new Source(source);
                let sourceKey = sourceItem.getKey(ctx);
                const buffer = await ctx.stub.getState(sourceKey);
                let asset = JSON.parse(buffer.toString());
                let trackedItem = new TrackedItem(asset, false);
                if (await trackedItem.applyRecursiveBooleanRule(ctx, rule)) {
                    return true;
                }
            }
        }
        return false;
    }
}

module.exports = TrackedItem;
