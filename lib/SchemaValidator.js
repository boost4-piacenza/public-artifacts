/*******************************************************************************
 * Copyright 2020 IBM
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
 "use strict";

const yaml = require("js-yaml");
const fs = require("fs");
const path = require("path");
let swaggerDocument = yaml.safeLoad(fs.readFileSync(path.resolve(__dirname, "swagger.yaml")));
let Validator = require("jsonschema").Validator;

function createValidationError(errors) {
    let errorsStringArray = errors.map(e => {
        return e.stack;
    });
    //TODO: Implement detailed errors
    return new Error(errorsStringArray.join(" , "));
}

class SchemaValidator {
    constructor(obj) {
        this.validator = new Validator();
        for (let defName of Object.keys(swaggerDocument.definitions)) {
            let schema = swaggerDocument.definitions[defName];
            schema.id = "#/definitions/" + defName;
            this.validator.addSchema(schema);
        }
    }

    validate(object, className) {
        if (!swaggerDocument.definitions[className]) {
            throw new Error("schema not found.");
        }
        let result = this.validator.validate(object, "#/definitions/" + className);
        if (result.errors.length === 0) {
            return true;
        }
        let validationError = createValidationError(result.errors);
        throw validationError;
    }
}
module.exports = SchemaValidator;
