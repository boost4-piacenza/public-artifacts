/*******************************************************************************
 * Copyright 2020 IBM
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
 "use strict";

let SchemaValidator = require("./SchemaValidator.js");
const validatorEngine = new SchemaValidator();

class Entity {
    constructor(obj, enableValidation) {
        if (enableValidation) {
            validatorEngine.validate(obj, this.constructor.name);
        }
    }

    toJson() {
        return JSON.parse(JSON.stringify(this));
    }

    toString() {
        return JSON.stringify(this);
    }

    static validate(object, classname) {
        return validatorEngine.validate(this.toJson(), this.constructor.name);
    }

    validate() {
        return validatorEngine.validate(this.toJson(), this.constructor.name);
    }
}

module.exports = Entity;
