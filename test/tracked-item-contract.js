/*******************************************************************************
 * Copyright 2020 IBM
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

"use strict";

const { ClientIdentity } = require("fabric-shim");
const { TrackedItemContract } = require("..");
const winston = require("winston");
const { ChaincodeMockStub } = require("@theledger/fabric-mock-stub");

const chai = require("chai");
const chaiAsPromised = require("chai-as-promised");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

const contract = new TrackedItemContract();
const TrackedItem = require("../lib/TrackedItem");
const Document = require("../lib/Document");

class TestContext {
    constructor() {
        this.stub = new ChaincodeMockStub("MyMockStub", contract);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logging = {
            getLogger: sinon
                .stub()
                .returns(
                    sinon.createStubInstance(winston.createLogger().constructor)
                ),
            setLevel: sinon.stub()
        };
    }
}

describe("TrackedItemContract", () => {
    let ctx;
    let asset = {
        ownerId: "acme",
        quantity: 2,
        unitMeasure: "kg",
        lotNumber: "D363G",
        taricCode: "234523",
        productCode: "fgw443",
        date: "2019-01-01",
        country: "ITA",
        sources: [],
        documents: []
    };

    beforeEach(() => {
        ctx = new TestContext();
        //ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"tracked item 1001 value"}'));
        //ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"tracked item 1002 value"}'));
    });

    afterEach(() => {
        sinon.restore();
    });

    describe("#assetExists", () => {
        it("should return true for a tracked item", async () => {
            sinon
                .stub(ctx.stub, "getState")
                .withArgs("1001")
                .resolves("{id: '1001'}");
            await contract.assetExists(ctx, "1001").should.eventually.be.true;
        });

        it("should return false for a tracked item that does not exist", async () => {
            sinon
                .stub(ctx.stub, "getState")
                .withArgs("1001")
                .resolves("{id: '1001'}");
            await contract.assetExists(ctx, "1003").should.eventually.be.false;
        });
    });

    describe("#readTrackedItem", () => {
        it("should return a tracked item", async () => {
            let temp = new TrackedItem(asset);
            sinon
                .stub(ctx.stub, "getState")
                .withArgs(temp.getKey(ctx))
                .resolves(JSON.stringify(asset));
            await contract
                .readTrackedItem(
                    ctx,
                    asset.ownerId,
                    asset.productCode,
                    asset.lotNumber
                )
                .should.eventually.deep.equal(asset);
        });

        it("should throw an error for a tracked item that does not exist", async () => {
            await contract
                .readTrackedItem(
                    ctx,
                    asset.ownerId,
                    asset.productCode,
                    asset.lotNumber
                )
                .should.be.rejectedWith(/The asset (.)* does not exist/);
        });
    });

    describe("#createTrackedItem", () => {
        it("should reject invalid input", async () => {
            let invalid = JSON.parse(JSON.stringify(asset));
            delete invalid.ownerId;
            await contract
                .createTrackedItem(ctx, JSON.stringify(invalid))
                .should.be.rejectedWith("instance requires property \"ownerId\"");
        });

        it("should reject invalid country type", async () => {
            let invalid = JSON.parse(JSON.stringify(asset));
            delete invalid.country;
            invalid.countryOfTransformation = "Italy";
            await contract
                .createTrackedItem(ctx, JSON.stringify(invalid))
                .should.be.rejectedWith(
                    "instance requires property \"country\" , instance additionalProperty \"countryOfTransformation\" exists in instance when not allowed"
                );
        });

        it("should create a tracked item", async () => {
            sinon.spy(ctx.stub, "putState");
            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(asset));
            ctx.stub.mockTransactionEnd();
            let temp = new TrackedItem(asset);
            ctx.stub.putState.should.have.been.calledOnceWithExactly(
                temp.getKey(ctx),
                Buffer.from(JSON.stringify(asset))
            );
        });

        it("should throw an error for a tracked item that already exists", async () => {
            let temp = new TrackedItem(asset);
            sinon
                .stub(ctx.stub, "getState")
                .withArgs(temp.getKey(ctx))
                .resolves("{id: '1003'}");
            ctx.stub.mockTransactionStart();
            await contract
                .createTrackedItem(ctx, JSON.stringify(asset))
                .should.be.rejectedWith(/The tracked item (.)* already exists/);
            ctx.stub.mockTransactionEnd();
        });

        it("should prevent me from using a non-existent source", async () => {
            let newAsset = JSON.parse(JSON.stringify(asset));
            newAsset.sources.push({
                ownerId: "abc",
                productCode: "123",
                lotNumber: "zzz",
                quantity: 7
            });

            ctx.stub.mockTransactionStart();
            await contract
                .createTrackedItem(ctx, JSON.stringify(newAsset))
                .should.be.rejectedWith(/Source (.)* does not exists/);
            ctx.stub.mockTransactionEnd();
        });

        it("should prevent me from consuming too much source quantity", async () => {
            let newAsset = JSON.parse(JSON.stringify(asset));
            newAsset.sources.push({
                ownerId: "abc",
                productCode: "123",
                lotNumber: "zzz",
                quantity: 7
            });

            let source1 = new TrackedItem({
                ownerId: "abc",
                productCode: "123",
                lotNumber: "zzz",
                quantity: 5
            });
            sinon
                .stub(ctx.stub, "getState")
                .withArgs(source1.getKey(ctx))
                .resolves(source1.toString());

            ctx.stub.mockTransactionStart();
            await contract
                .createTrackedItem(ctx, JSON.stringify(newAsset))
                .should.be.rejectedWith(/Overconsumption of (.)*/);
            ctx.stub.mockTransactionEnd();
        });

        it("should prevent me from consuming the same source twice", async () => {
            let newAsset = JSON.parse(JSON.stringify(asset));
            newAsset.sources.push(
                {
                    ownerId: "abc",
                    productCode: "123",
                    lotNumber: "zzz",
                    quantity: 7
                },
                {
                    ownerId: "abc",
                    productCode: "123",
                    lotNumber: "zzz",
                    quantity: 12
                }
            );

            let source1 = new TrackedItem({
                ownerId: "abc",
                productCode: "123",
                lotNumber: "zzz",
                quantity: 500
            });
            sinon
                .stub(ctx.stub, "getState")
                .withArgs(source1.getKey(ctx))
                .resolves(source1.toString());

            ctx.stub.mockTransactionStart();
            await contract
                .createTrackedItem(ctx, JSON.stringify(newAsset))
                .should.be.rejectedWith(
                    /Cannot use the same source twice. (.)* was already seen/
                );
            ctx.stub.mockTransactionEnd();
        });

        it("should reduce quantity of all sources", async () => {
            let source1 = JSON.parse(JSON.stringify(asset));
            source1.ownerId = "abc";
            source1.productCode = "123";
            source1.lotNumber = "zzz";
            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(source1));
            ctx.stub.mockTransactionEnd();

            let source2 = JSON.parse(JSON.stringify(asset));
            source2.ownerId = "abc";
            source2.productCode = "456";
            source2.lotNumber = "zzz";
            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(source2));
            ctx.stub.mockTransactionEnd();

            let newAsset = JSON.parse(JSON.stringify(asset));
            newAsset.sources.push({
                ownerId: "abc",
                productCode: "123",
                lotNumber: "zzz",
                quantity: 1
            });
            newAsset.sources.push({
                ownerId: "abc",
                productCode: "456",
                lotNumber: "zzz",
                quantity: 2
            });
            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(newAsset));
            ctx.stub.mockTransactionEnd();

            let updatedSource1 = await contract.readTrackedItem(
                ctx,
                "abc",
                "123",
                "zzz"
            );
            updatedSource1.quantity.should.equal(1);

            let updatedSource2 = await contract.readTrackedItem(
                ctx,
                "abc",
                "456",
                "zzz"
            );
            updatedSource2.quantity.should.equal(0);
        });

        it("should prevent using the same document twice", async () => {
            let newAsset = JSON.parse(JSON.stringify(asset));
            newAsset.documents.push({
                producer: "abc",
                type: "INVOICE",
                number: "qwe",
                date: "2019-01-01",
                hash: "ppp",
                hash_algorithm: "md5",
                fileLocation: "path/to"
            });
            newAsset.documents.push({
                producer: "abc",
                type: "INVOICE",
                number: "qwe",
                date: "2019-01-01",
                hash: "ppp",
                hash_algorithm: "md5",
                fileLocation: "path/to"
            });

            ctx.stub.mockTransactionStart();
            await contract
                .createTrackedItem(ctx, JSON.stringify(newAsset))
                .should.be.rejectedWith(/Same document (.)* referenced twice/);
            ctx.stub.mockTransactionEnd();
        });

        it("Should call createDocument", async () => {
            sinon.spy(contract, "createDocument");

            let newAsset = JSON.parse(JSON.stringify(asset));
            newAsset.documents.push({
                producer: "abc",
                type: "INVOICE",
                number: "qwe2345234",
                date: "2019-01-01",
                hash: "ppp",
                hash_algorithm: "md5",
                fileLocation: "path/to"
            });
            newAsset.documents.push({
                producer: "abc",
                type: "INVOICE",
                number: "qwe76543",
                date: "2019-01-01",
                hash: "ppp",
                hash_algorithm: "md5",
                fileLocation: "path/to"
            });

            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(newAsset));
            ctx.stub.mockTransactionEnd();

            contract.createDocument.should.have.been.calledTwice;
        });
    });

    describe("#createDocument", () => {
        it("should prevent storing an incomplete document", async () => {
            let doc = {
                producer: "abc",
                type: "INVOICE",
                number: "qwe",
                date: "2019-01-01",
                fileLocation: "path/to"
            }; //Missing hash

            ctx.stub.mockTransactionStart();
            await contract
                .createDocument(ctx, JSON.stringify(doc))
                .should.be.rejectedWith(/Document (.)* is incomplete/);
            ctx.stub.mockTransactionEnd();
        });

        it("should prevent to use existing document with different values", async () => {
            let document = new Document({
                producer: "abc",
                type: "INVOICE",
                number: "qwe",
                date: "2019-01-01",
                hash: "ppp",
                hash_algorithm: "md5",
                fileLocation: "path/to"
            });

            sinon
                .stub(ctx.stub, "getState")
                .withArgs(document.getKey(ctx))
                .resolves(document.toString());

            let newAsset = {
                producer: "abc",
                type: "INVOICE",
                number: "qwe",
                date: "2019-01-01",
                hash: "BAD HASH",
                hash_algorithm: "md5",
                fileLocation: "path/to"
            };

            ctx.stub.mockTransactionStart();
            await contract
                .createDocument(ctx, JSON.stringify(newAsset))
                .should.be.rejectedWith(/(.)* already exists and is different/);
            ctx.stub.mockTransactionEnd();
        });

        it("should store documents", async () => {
            let doc1 = {
                producer: "abc",
                type: "INVOICE",
                number: "qwe",
                date: "2019-01-01",
                hash: "ppp",
                hash_algorithm: "md5",
                fileLocation: "path/to"
            };

            ctx.stub.mockTransactionStart();
            await contract.createDocument(ctx, JSON.stringify(doc1));
            ctx.stub.mockTransactionEnd();

            let storedDoc1 = await contract.readDocument(
                ctx,
                "abc",
                "INVOICE",
                "qwe"
            );
            storedDoc1.should.deep.equal(doc1);
        });

        it("should allow storing a document even if it already exists, incomplete data", async () => {
            let doc1 = {
                producer: "abc",
                type: "INVOICE",
                number: "qwe",
                date: "2019-01-01",
                hash: "ppp",
                hash_algorithm: "md5",
                fileLocation: "path/to"
            };

            ctx.stub.mockTransactionStart();
            await contract.createDocument(ctx, JSON.stringify(doc1));
            ctx.stub.mockTransactionEnd();

            let storedDoc1 = await contract.readDocument(
                ctx,
                "abc",
                "INVOICE",
                "qwe"
            );
            storedDoc1.should.deep.equal(doc1);

            let doc2 = {
                producer: "abc",
                type: "INVOICE",
                number: "qwe"
            }; //Just enough data to be valid
            ctx.stub.mockTransactionStart();
            await contract.createDocument(ctx, JSON.stringify(doc2));
            ctx.stub.mockTransactionEnd();

            storedDoc1 = await contract.readDocument(
                ctx,
                "abc",
                "INVOICE",
                "qwe"
            );
            storedDoc1.should.deep.equal(doc1);
        });

        it("should allow storing a document even if it already exists, exact copy", async () => {
            let doc1 = {
                producer: "abc",
                type: "INVOICE",
                number: "qwe",
                date: "2019-01-01",
                hash: "ppp",
                hash_algorithm: "md5",
                fileLocation: "path/to"
            };

            ctx.stub.mockTransactionStart();
            await contract.createDocument(ctx, JSON.stringify(doc1));
            ctx.stub.mockTransactionEnd();

            let storedDoc1 = await contract.readDocument(
                ctx,
                "abc",
                "INVOICE",
                "qwe"
            );
            storedDoc1.should.deep.equal(doc1);

            ctx.stub.mockTransactionStart();
            await contract.createDocument(ctx, JSON.stringify(doc1));
            ctx.stub.mockTransactionEnd();

            storedDoc1 = await contract.readDocument(
                ctx,
                "abc",
                "INVOICE",
                "qwe"
            );
            storedDoc1.should.deep.equal(doc1);
        });
    });

    describe("#addDocumentToTrackedItem", () => {
        it("should fail with a non-existing item", async () => {
            await contract
                .addDocumentToTrackedItem(ctx, "aa", "aa", "aa", "")
                .should.be.rejectedWith(/The asset (.)* does not exist/);
        });

        it("should fail with an invalid document", async () => {
            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(asset));
            ctx.stub.mockTransactionEnd();

            await contract
                .addDocumentToTrackedItem(
                    ctx,
                    asset.ownerId,
                    asset.productCode,
                    asset.lotNumber,
                    "{}"
                )
                .should.be.rejectedWith(/instance requires property/);
        });

        it("should fail with an incomplete document", async () => {
            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(asset));
            ctx.stub.mockTransactionEnd();

            let doc = {
                producer: "abc",
                type: "INVOICE",
                number: "qwe",
                date: "2019-01-01",
                fileLocation: "path/to"
            }; //Missing hash

            await contract
                .addDocumentToTrackedItem(
                    ctx,
                    asset.ownerId,
                    asset.productCode,
                    asset.lotNumber,
                    JSON.stringify(doc)
                )
                .should.be.rejectedWith(/Document (.)* is incomplete/);
        });

        it("should fail with a duplicate document", async () => {
            let newAsset = JSON.parse(JSON.stringify(asset));
            newAsset.documents.push({
                producer: "abc",
                type: "INVOICE",
                number: "qwe2345234",
                date: "2019-01-01",
                hash: "ppp",
                hash_algorithm: "md5",
                fileLocation: "path/to"
            });

            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(newAsset));
            ctx.stub.mockTransactionEnd();

            let doc = {
                producer: "abc",
                type: "INVOICE",
                number: "qwe2345234",
                date: "2019-01-01",
                hash: "ppp",
                hash_algorithm: "md5",
                fileLocation: "path/to"
            };

            ctx.stub.mockTransactionStart();
            await contract
                .addDocumentToTrackedItem(
                    ctx,
                    asset.ownerId,
                    asset.productCode,
                    asset.lotNumber,
                    JSON.stringify(doc)
                )
                .should.be.rejectedWith(/Same document (.)* referenced twice/);
            ctx.stub.mockTransactionEnd();
        });

        it("should work with a complete document", async () => {
            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(asset));
            ctx.stub.mockTransactionEnd();

            let doc = {
                producer: "abc",
                type: "INVOICE",
                number: "qwe2345234",
                date: "2019-01-01",
                hash: "ppp",
                hash_algorithm: "md5",
                fileLocation: "path/to"
            };

            ctx.stub.mockTransactionStart();
            await contract.addDocumentToTrackedItem(
                ctx,
                asset.ownerId,
                asset.productCode,
                asset.lotNumber,
                JSON.stringify(doc)
            );
            ctx.stub.mockTransactionEnd();

            let storedDoc = await contract.readDocument(
                ctx,
                doc.producer,
                doc.type,
                doc.number
            );
            storedDoc.should.deep.equal(doc);

            let trackedItem = await contract.readTrackedItem(
                ctx,
                asset.ownerId,
                asset.productCode,
                asset.lotNumber
            );
            let expectedAsset = JSON.parse(JSON.stringify(asset));
            expectedAsset.documents = [doc];

            trackedItem.should.deep.equal(expectedAsset);
        });

        it("should work with an incomplete existing document", async () => {
            let doc = {
                producer: "abc",
                type: "INVOICE",
                number: "qwe2345234",
                date: "2019-01-01",
                hash: "ppp",
                hash_algorithm: "md5",
                fileLocation: "path/to"
            };

            ctx.stub.mockTransactionStart();
            await contract.createDocument(ctx, JSON.stringify(doc));
            ctx.stub.mockTransactionEnd();

            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(asset));
            ctx.stub.mockTransactionEnd();

            ctx.stub.mockTransactionStart();
            await contract.addDocumentToTrackedItem(
                ctx,
                asset.ownerId,
                asset.productCode,
                asset.lotNumber,
                JSON.stringify({
                    producer: doc.producer,
                    type: doc.type,
                    number: doc.number
                })
            );
            ctx.stub.mockTransactionEnd();

            let storedDoc = await contract.readDocument(
                ctx,
                doc.producer,
                doc.type,
                doc.number
            );
            storedDoc.should.deep.equal(doc);

            let trackedItem = await contract.readTrackedItem(
                ctx,
                asset.ownerId,
                asset.productCode,
                asset.lotNumber
            );
            let expectedAsset = JSON.parse(JSON.stringify(asset));
            expectedAsset.documents = [doc];

            trackedItem.should.deep.equal(expectedAsset);
        });
    });

    describe("#applyRecursiveBooleanRule", () => {
        it("should find the correct field", async () => {
            let source1 = JSON.parse(JSON.stringify(asset));
            source1.ownerId = "abc";
            source1.productCode = "123";
            source1.lotNumber = "zzz";
            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(source1));
            ctx.stub.mockTransactionEnd();

            let source2 = JSON.parse(JSON.stringify(asset));
            source2.ownerId = "abc";
            source2.productCode = "456";
            source2.lotNumber = "zzz";
            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(source2));
            ctx.stub.mockTransactionEnd();

            let newAsset = JSON.parse(JSON.stringify(asset));
            newAsset.sources.push({
                ownerId: "abc",
                productCode: "123",
                lotNumber: "zzz",
                quantity: 1
            });
            newAsset.sources.push({
                ownerId: "abc",
                productCode: "456",
                lotNumber: "zzz",
                quantity: 2
            });
            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(newAsset));
            ctx.stub.mockTransactionEnd();

            let trackedItem = new TrackedItem(newAsset);
            let result = await trackedItem.applyRecursiveBooleanRule(
                ctx,
                asset => asset.ownerId === "acme"
            );
            result.should.equal(true);

            result = await trackedItem.applyRecursiveBooleanRule(
                ctx,
                asset => asset.ownerId === "abc"
            );
            result.should.equal(true);

            result = await trackedItem.applyRecursiveBooleanRule(
                ctx,
                asset => asset.ownerId === "BADUSER"
            );
            result.should.equal(false);
        });

        it("should reject Yemen to USA", async () => {
            let source1 = JSON.parse(JSON.stringify(asset));
            source1.ownerId = "abc";
            source1.productCode = "123";
            source1.lotNumber = "zzz";
            source1.country = "YEM";
            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(source1));
            ctx.stub.mockTransactionEnd();

            let source2 = JSON.parse(JSON.stringify(asset));
            source2.ownerId = "abc";
            source2.productCode = "456";
            source2.lotNumber = "zzz";
            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(source2));
            ctx.stub.mockTransactionEnd();

            let newAsset = JSON.parse(JSON.stringify(asset));
            newAsset.country = "USA";
            newAsset.sources.push({
                ownerId: "abc",
                productCode: "123",
                lotNumber: "zzz",
                quantity: 1
            });
            newAsset.sources.push({
                ownerId: "abc",
                productCode: "456",
                lotNumber: "zzz",
                quantity: 2
            });
            ctx.stub.mockTransactionStart();
            await contract
                .createTrackedItem(ctx, JSON.stringify(newAsset))
                .should.be.rejectedWith(/Transaction breaks law #12345/);
            ctx.stub.mockTransactionEnd();
        });

        it("should allow Yemen to ITA", async () => {
            let source1 = JSON.parse(JSON.stringify(asset));
            source1.ownerId = "abc";
            source1.productCode = "123";
            source1.lotNumber = "zzz";
            source1.country = "YEM";
            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(source1));
            ctx.stub.mockTransactionEnd();

            let source2 = JSON.parse(JSON.stringify(asset));
            source2.ownerId = "abc";
            source2.productCode = "456";
            source2.lotNumber = "zzz";
            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(source2));
            ctx.stub.mockTransactionEnd();

            let newAsset = JSON.parse(JSON.stringify(asset));
            newAsset.sources.push({
                ownerId: "abc",
                productCode: "123",
                lotNumber: "zzz",
                quantity: 1
            });
            newAsset.sources.push({
                ownerId: "abc",
                productCode: "456",
                lotNumber: "zzz",
                quantity: 2
            });
            ctx.stub.mockTransactionStart();
            await contract.createTrackedItem(ctx, JSON.stringify(newAsset));
            ctx.stub.mockTransactionEnd();
        });
    });
});
