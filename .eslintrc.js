/*******************************************************************************
 * Copyright 2020 IBM
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

module.exports = {
    env: {
        node: true,
        mocha: true,
        es6: true
    },
    parserOptions: {
        ecmaVersion: 8,
        sourceType: "script"
    },
    extends: "eslint:recommended",
    rules: {
        "no-unused-vars": ["error", { args: "none" }],
        "no-console": "off",
        curly: "error",
        eqeqeq: "error",
        "no-throw-literal": "error",
        strict: "error",
        "no-var": "error",
        "dot-notation": "error",
        "no-use-before-define": "error",
        "no-useless-call": "error",
        "no-with": "error",
        yoda: "error",
        "quote-props": ["error", "as-needed"],
        "no-constant-condition": ["error", { checkLoops: false }]
    }
};
